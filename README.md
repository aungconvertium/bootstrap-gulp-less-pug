# Bootstrap Scaffold
Read this documentation before starting the project.

## Important
Change `pkg.name` from `package.json` and `bower.json` to your *project name*.
It will be used as your project css and js file names in Gulpfile.
Change `build:css` and `build:js` file names same as `pkg.name`.

## Install
`npm install`
`bower install`

## Build
`gulp`

## Watch 
`gulp watch`

## Js lint
`gulp lint`
